<?php
    $host = "localhost";
    $username = "phpmyadmin";
    $password = "root";
    $dbname = "deploy_monitoringiccu";

    $conn = new mysqli($host, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("connection error: ".$conn->connect_error);
    }
    return $conn;
?>
