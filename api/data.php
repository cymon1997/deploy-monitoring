<?php
    require 'index.php';

    $conn = connect();
    $result->heartbeat = getLastBeat($conn);
    $result->temperature = getLastTemp($conn);

    $conn->close();
    echo json_encode($result);
?>
