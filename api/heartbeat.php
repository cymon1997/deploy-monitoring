<?php
    require 'index.php';

    $conn = connect();
    $result->heartbeat = getLastBeat($conn);

    $conn->close();
    return json_encode($result);
?>
