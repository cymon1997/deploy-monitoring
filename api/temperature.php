<?php
    require 'index.php';

    $conn = connect();
    $result->temperature = getLastTemp($conn);

    $conn->close();
    return json_encode($result);
?>
