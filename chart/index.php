<?php
 ?>
 <link href="css/monitor.css" rel="stylesheet">

 <div class="bg" style="margin-top: 35px;">
     <h3 id="heartbeat-title">Heartbeat</h3>
     <div class="row">
         <div class="col-sm-10">
             <div id="heartbeat" class="ct-chart"></div>
         </div>
         <div class="col-sm-2">
             <h1 id="heartbeat-current">0</h1>
         </div>
     </div>
     <h3 id="temperature-title">Temperature</h3>
     <div class="row">
         <div class="col-sm-10">
             <div id="temperature" class="ct-chart"></div>
         </div>
         <div class="col-sm-2">
             <h1 id="temperature-current">0</h1>
         </div>
     </div>
 </div>

 <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
 <script src="js/chartist.min.js"></script>
 <script src="js/monitor.js"></script>
